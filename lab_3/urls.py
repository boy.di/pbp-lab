from django import urls
from django.contrib import admin
from django.contrib.admin import sites
from django.urls import path
from . import views

urlpatterns = [
    path('add/', views.add_friend, name='add'),
    path('', views.index, name='index'),
]