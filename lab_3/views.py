from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    form = FriendForm(request.POST or None)

    # cek jika form valid
    if form.is_valid() and request.method == 'POST':
        # menyimpan data form ke model
        form.save()
        return HttpResponseRedirect('/lab-3/')

    context = {
        'form' : form
    }
    return render(request, "lab3_form.html", context)
