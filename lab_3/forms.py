from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = [
            'name',
            'npm',
            'DOB',
        ]
    name = forms.CharField(max_length=40, widget=forms.TextInput({'type':'text', 'placeholder':'Masukkan nama'}))
    npm = forms.CharField(max_length=40, widget=forms.TextInput({'type':'text', 'placeholder':'Masukkan npm'}))
    DOB = forms.CharField(max_length=40, widget=forms.TextInput({'type':'text', 'placeholder':'Masukkan tanggal lahir'}))