from django.shortcuts import render
from . import models
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    notes = models.Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', models.Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', models.Note.objects.all())
    return HttpResponse(data, content_type="application/json")