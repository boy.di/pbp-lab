1. Apakah perbedaan antara JSON dan XML?  
Perbedaan:
    1. Bahasa:  
        XML : Merupakan bahasa mark-up, bukan bahasa pemrograman  
        JSON: Menggunakan JavaScript  
    2. Ukuran dokumen:  
        XML: Besar karena struktur tag  
        JSON: Ukuran data kecil  
    3. Kecepatan:  
        XML: Transmisi data lebih lambat dikarenakan ukuran file yang besar  
        JSON: Transmisi data lebih cepat dikarenakan ukuran file yang kecil  
    4. Penyimpanan Data:  
        XML: Disimpan sebagai tree structure  
        JSON: Menyimpan data seperti map (pasangan dari key dan value)  
  
2. Apakah perbedaan antara HTML dan XML?  
Perbedaan:
    1. Fokus utama:  
        XML: Berfokus pada transfer data  
        HTML: Berfokus untuk menampilkan data  
    2. Dukungan namespaces  
        XML: Menyediakan dukungan namespaces  
        HTML: Tidak menyediakan dukungan namespaces  
    3. Tag Penutup  
        XML: Harus ada tag penutup  
        HTML: Beberapa elemen bisa digunakan tanpa tag penutup  
    4. Case sensitive  
        XML: Case sensitive  
        HTML: Case insensitive  
    5. Penggunaan tag:  
        XML: Mendefinisikan sendiri kumpulan tag nya  
        HTML: Tag nya sudah ditentukan sebelumnya  