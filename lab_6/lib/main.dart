import 'package:flutter/material.dart';

import 'form_user.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: MyHome(), theme: ThemeData(primarySwatch: Colors.blueGrey));
  }
}

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(197, 203, 212, 0.4),
      appBar: AppBar(
        title: const Center(child: Text("Infid - Informasi Covid")),
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            const Center(
                child: Text(
              "Daftar Rumah Sakit",
              style: TextStyle(fontWeight: FontWeight.bold),
            )),
            const Text("\n"),
            buatCard(
                "RSK Pusat Otak Nasional",
                "Jl MT Haryono Kav 11, Kelurahan Cawang, Kecamatan Kramat Jati, Jakarta",
                "02122040055",
                context),
            buatCard("RS Jantung dan Pembuluh Darah Harapan kita",
                "Jl. S. Parman Kav 87, Slipi, Jakarta", "02156824241", context),
            buatCard("RS Penyakit Infeksi Prof. Dr. Sulianti Saroso",
                "Jl. Baru Sunter Permai Raya Jakarta", "0216506559", context),
            buatCard(
                "RS Kanker Dharmais",
                "Jl. S Parman Kav.84-86 Slipi Jakarta",
                "081318238366",
                context),
            buatCard(
                "RS Umum Daerah Kebayoran Lama",
                "Jl. Jatayu RW. 001 RT. 012 Kelurahan Kebayoran Lama Selatan Kecamatan Kebayoran Lama, Jakarta",
                "02127939067",
                context),
          ],
        ),
      ),
    );
  }

  Card buatCard(
      String rumahSakit, String alamat, String noTelfon, BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 17, left: 15, right: 15, bottom: 20),
            child: Text(
              rumahSakit,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 17, left: 15, right: 50, bottom: 12),
            child: Text(alamat),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 50, bottom: 10),
            child: Text(
              "No Telfon: " + noTelfon,
              style: const TextStyle(fontStyle: FontStyle.italic, fontSize: 10),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10, left: 15, right: 50, bottom: 10),
            child: ElevatedButton(
              child: const Text(
                "Book",
                style: TextStyle(fontSize: 15),
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const FormUser()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
