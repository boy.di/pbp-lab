import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormUser extends StatelessWidget {
  const FormUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.blueGrey),
        home: Scaffold(
          backgroundColor: const Color.fromRGBO(197, 203, 212, 0.4),
          appBar: AppBar(
            title: const Center(child: Text("Booking Rumah Sakit")),
          ),
          body: Container(
              margin: const EdgeInsets.all(25),
              child: ListView(
                children: <Widget>[
                  const Center(
                      child: Text("Form Booking\n",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20))),
                  const Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 5),
                    child: Text("Nama"),
                  ),
                  const TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Masukkan nama'),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 12, bottom: 5),
                    child: Text("Umur"),
                  ),
                  const TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Masukkan umur'),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 5),
                    child: Text("No Telfon"),
                  ),
                  const TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Masukkan nomor telfon'),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 5),
                    child: Text("Rumah Sakit"),
                  ),
                  const TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Rumah sakit yang dituju'),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: ElevatedButton(
                      child: const Text("Submit"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: ElevatedButton(
                      child: const Text("Back"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              )),
        ));
  }
}
