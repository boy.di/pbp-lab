from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('friendlist/', friend_list, name='friend_list'),
]
