from django.shortcuts import render
from lab_2.models import *

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab5_index.html', response)

def get_note(request):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # add the dictionary during initialization
    context= {"notes" : Note.objects.get(id = id)}
         
    return render(request, "list_view.html", context)
