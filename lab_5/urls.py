from django.urls import path
from .views import *

urlpatterns = [
    path('<id>', get_note, name="single_note"),
    path('', index, name="index")
]