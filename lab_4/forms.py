from django import forms
from django.forms import widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = [
            'to',
            'From',
            'title',
            'message',
        ]
        labels = {
            'to' : 'Ke',
            'From' : 'Dari',
            'title' : 'Judul Pesan',
            'message' : 'Pesan',
        }
        widgets = {
            'to' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan nama tujuan'}),
            'From' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan nama pengirim'}),
            'title' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan judul'}),
            'message' : forms.Textarea(attrs={'class' : 'form-control', 'placeholder' : 'Masukkan pesan'}),
        }