from django import urls
from django.contrib import admin
from django.contrib.admin import sites
from django.urls import path
from .views import *

urlpatterns = [
    path('add-note/', add_note, name="tambah_note"),
    path('note-list/', note_list, name="card note"),
    path('', index, name='index'),
]