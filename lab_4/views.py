from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all()
    context = {'notes' : notes}
    return render(request, 'lab4_index.html', context)

def add_note(request):
    # request.POST adalah method dari html
    form = NoteForm(request.POST or None)

    # cek jika form valid
    # request.method = post request dari browser
    if form.is_valid() and request.method == 'POST':
        # menyimpan data form ke model
        form.save()
        return HttpResponseRedirect('/lab-4/')

    context = {
        'forms' : form
    }
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    context = {'notes' : notes}
    return render(request, 'lab4_note_list.html', context)